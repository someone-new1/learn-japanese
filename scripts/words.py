import pandas as pd
import numpy as np
import os

os.system('chcp 932')


# take info
print("Words or letters?('words', 'new', 'old', ...) --> ", end=' ')
whats = input().lower()

print("How much words(number, 'all') --> ", end=' ')
quantity = input()



from learning import Learn

# take path to csv
if whats == 'words':
    path = '../csv/words.csv'

elif whats == 'hiragana':
    path = '../csv/hiragana.csv'

elif whats == 'katakana':
    path = '../csv/katakana.csv'

else:
    path = f'../csv/{whats}_words.csv'


print("What's you want to predict?:", end=' ')
to_predict = input().lower().strip()

if 'eng' in to_predict:
    to_predict = 'translate'
elif 'jap' in to_predict:
    to_predict = 'jap'

learn_words = Learn(path, to_predict)
learn_words.random_words(quantity=quantity)
print(learn_words.learning())


# logic to save into file
if whats == 'words':
    if learn_words.score / len(learn_words.file) == 1:
        print('Do you want to add this words to learned?("yes", "no") -->', end=' ')
        add = input().lower()
        print('And delete words from here? -->', end=' ')
        delete = input().lower()
        
        if add == 'yes':
            if delete == 'yes':
                learn_words.update('../csv/old_words.csv', delete=True)
            else:
                learn_words.update('../csv/old_words.csv')
    
elif whats == 'old':
    learn_words.update('../csv/repeat_words.csv', incorrect=True)

elif whats == 'repeat':
    learn_words.drop_from_repeat()

os.system('chcp 866')