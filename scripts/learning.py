import pandas as pd
import numpy as np






class Learn:
    def __init__(self, current_path, to_predict, input=''):
        self.score = 0
        self.current_path = current_path
        self.file = pd.read_csv(current_path)
        self.to_predict = to_predict
        if input == '':
            self.input_data = [col for col in self.file.columns if col != to_predict][0]
        else:
            self.input_data = input
        # create DataFrame for incorrect answers and create in this DataFrame empty columns
        self.incorrect = pd.DataFrame()
        self.incorrect[self.to_predict] = []

    def random_words(self, quantity, before=True):
        if quantity == 'all':
            quantity = len(self.file)
        else:
            quantity = int(quantity)

        # first we randomized quantity of words, then take quantity of already randomized words
        if before == True:
            indexes = self.file.index.to_list()
            np.random.shuffle(indexes)
            indexes = indexes[:quantity]
            self.file = self.file.loc[indexes]
        # first we take quantity of words, then randomized them
        else:
            self.file = self.file.iloc[-1:-quantity-1:-1]
            indexes = self.file.index.to_list()
            np.random.shuffle(indexes)
            self.file = self.file.loc[indexes]

        self.file.index = [i for i in range(len(self.file))]

    def learning(self):
        i = 0
        for ind in range(len(self.file)):
            print(self.file[self.input_data][ind], '--> ', end='')
            if input() in self.file[self.to_predict][ind].split(';'):
                self.score += 1
            else:
                print(f'False answer, true is: {self.file[self.to_predict][ind]}')
                self.incorrect.loc[i, self.to_predict] = self.file[self.to_predict][ind]
                self.incorrect.loc[i, self.input_data] = self.file[self.input_data].loc[ind]
                i += 1

        return f'Scores: {self.score} of {len(self.file)}. It is {round(self.score/len(self.file)* 100, 2)}%'

    def update(self, save_to, incorrect=False, delete=False):
        to_concat = pd.read_csv(save_to)
        if incorrect:
            ready_to_save = pd.concat([self.incorrect, to_concat]).drop_duplicates()
            to_delete = self.incorrect
        else:
            ready_to_save = pd.concat([self.file, to_concat]).drop_duplicates()
            to_delete = self.file

        if 'words' in save_to:
            ready_to_save = ready_to_save.groupby('jap').agg(';'.join)
            ready_to_save.reset_index(inplace=True)
            on_delete = 'jap'
        elif 'kanji' in save_to:
            ready_to_save = ready_to_save.groupby('kanji').agg(';'.join)
            ready_to_save.reset_index(inplace=True)
            on_delete = 'kanji'

        ready_to_save.to_csv(save_to, index=False)

        if delete:
            current_file = pd.read_csv(self.current_path)
            changed_file = current_file[~current_file[on_delete].isin(to_delete[on_delete])]
            changed_file.to_csv(self.current_path, index=False)

    def drop_from_repeat(self):
        repeat = pd.read_csv(self.current_path)
        repeat = repeat[repeat[self.to_predict].isin(self.incorrect[self.to_predict])]
        repeat.to_csv(self.current_path, index=False)

    def random_words_from_new(self):
        pass
