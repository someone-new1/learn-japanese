import pandas as pd
from learning import Learn
import os

os.system('chcp 932')

# take info
print("Learn kanji or repeat?('kanji', 'old', 'new', 'repeat') --> ", end=' ')
whats = input().lower()

if whats == 'kanji':
    path = '../csv/kanji.csv'
else:
    path = f'../csv/{whats}_kanji.csv'


print('How much words(some num or "all") --> ', end='')
quantity = input()

print('What we want to predict --> meaning, hiragana, kanji: ', end='')
to_predict = input()

print('Input --> meaning, hiragana, kanji: ', end='')
input_data = input()


learn_kanji = Learn(path, to_predict, input_data)
learn_kanji.random_words(quantity=quantity)
print(learn_kanji.learning())


if whats == 'kanji':
    if learn_kanji.score / len(learn_kanji.file) == 1:
        print('Do you want to add this kanji to learned?("yes", "no") -->', end=' ')
        add = input().lower()
        print('And you want to delete this kanji from here? -->', end=' ')
        delete = input().lower()

        if add == 'yes':
            if delete == 'yes':
                learn_kanji.update('../csv/old_kanji.csv', delete=True)
            else:
                learn_kanji.update('../csv/old_kanji.csv')

elif whats == 'old':
    learn_kanji.update('../csv/repeat_kanji.csv', incorrect=True)

elif whats == 'repeat':
    learn_kanji.drop_from_repeat()

os.system('chcp 866')