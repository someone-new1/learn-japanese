import pandas as pd
import numpy as np
import os

os.system('chcp 932')

# read csv files
new_words = pd.read_csv('../csv/new_words.csv')
words = pd.read_csv('../csv/words.csv')

# take info
print('How much words --> ', end=' ')
how_much = int(input())

# shuffle values
np.random.shuffle(new_words.values)


# take this values and insert into words
values = new_words[:how_much]
words = pd.concat([values, words]).drop_duplicates()
new_words = new_words.drop(labels=list(values.index))
words.set_index('jap', inplace=True)
new_words.set_index('jap', inplace=True)


# save results
words.to_csv('../csv/words.csv')
new_words.to_csv('../csv/new_words.csv')

os.system('chcp 866')